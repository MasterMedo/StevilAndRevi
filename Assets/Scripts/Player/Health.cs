﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{

    public enum DeathAction { loadLevelWhenDead, doNothingWhenDead };

    public float HealthPoints = 100.0f;

    public int NumberOfLives = 1;
    public bool IsAlive = true;

    public GameObject ExplosionPrefab;

    public DeathAction OnLivesGone = DeathAction.doNothingWhenDead;

    public string LevelToLoad = "";

    private Vector3 _respawnPosition;
    private Quaternion _respawnRotation;


    // Use this for initialization
    void Start()
    {
        // store initial position as respawn location
        _respawnPosition = transform.position;
        _respawnRotation = transform.rotation;

        RespawnHealthPoints = HealthPoints;

        if (LevelToLoad == "") // default to current scene 
        {
            LevelToLoad = SceneManager.GetActiveScene().name;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (HealthPoints <= 0)
        {               // if the object is 'dead'
            NumberOfLives--; // decrement # of lives, update lives GUI

            if (ExplosionPrefab != null)
            {
                Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
            }

            if (NumberOfLives > 0)
            { // respawn
                transform.position = _respawnPosition;   // reset the player to respawn position
                transform.rotation = _respawnRotation;
                HealthPoints = RespawnHealthPoints;	// give the player full health again
            }
            else
            { // here is where you do stuff once ALL lives are gone)
                IsAlive = false;

                switch (OnLivesGone)
                {
                    case DeathAction.loadLevelWhenDead:
                        SceneManager.LoadScene(LevelToLoad);
                        break;
                    case DeathAction.doNothingWhenDead:
                        // do nothing, death must be handled in another way elsewhere
                        break;
                }
                Destroy(gameObject);
            }
        }
    }

    public float RespawnHealthPoints { get; private set; }

    public void ApplyDamage(float amount)
    {
        HealthPoints -= amount;
    }

    public void ApplyHeal(float amount)
    {
        HealthPoints += amount;
    }

    public void ApplyBonusLife(int amount)
    {
        NumberOfLives += amount;
    }

    public void UpdateRespawn(Vector3 newRespawnPosition, Quaternion newRespawnRotation)
    {
        _respawnPosition = newRespawnPosition;
        _respawnRotation = newRespawnRotation;
    }
}
