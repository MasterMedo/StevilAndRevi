﻿using System;
using System.ComponentModel;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class PlayerMovement : MonoBehaviour
{
    protected Rigidbody2D Rb2D;
   
    protected Vector3 Velocity;

    //u ovakvoj implementaciji mi neki nisu trebali
    protected bool FaceDirection { get; set; } 
    //protected bool IsGrounded { get; set; }
    //protected bool IsInAir { get; set; } 
    //protected bool IsOnWall { get; set; } 
    protected bool CanJump { get; set; } 
    protected bool CanDoubleJump { get; set; } 
    //protected bool UsedDoubleJump { get; set; } 
    protected bool CanTripleJump { get; set; } 
    //protected bool UsedTripleJump { get; set; } 
    protected bool CanDash { get; set; } 
    protected bool CanFloat { get; set; }
    protected bool IsFloating { get; set; }


    /*raznorazni parametri, prilicno self-explanatory, al za svaki slucaj, evo:
      JumpHeigh/TimeToJumpApex : u principu parametri parabole skoka, znace u biti "koliko visoko" i "koliko daleko"
      SecondaryJumpsFactor : faktor za visinu drugog i treceg skoka
      AccelerationTime: parametar krivulje koja smootha promjenu brzine, veci parametar = sporija promjena, cime
                        daje osjecaj vece "tezine", odnosno inercije
      MoveSpeed/DashVelocity : brzina hodanja i brzina dasha
      FiniteFloatSpeed : horizontalno i vertikalno ogranicenje na brzinu kod "padobrana"  
    */
    public float JumpHeight = 4;
    public float TimeToJumpApex = .4f;

    public float SecondaryJumpsFactor = 0.7f;

    
    public float AccelerationTimeGrounded = .3f;
    public float AccelerationTimeAirborne = .6f;
    public float AccelerationTimeFloating = .9f;

    public float MoveSpeed = 6;

    public float DashVelocity = 12;
    public float FiniteFloatVerticalSpeed = 2;
    public float FiniteFloatHorizontalSpeed = 2;

    private float _gravity;
    private float _jumpVelocity;
   
    private float _velocityXSmoothing;

    private Controller2D _controller;

    
    private void OnEnable()
    {
        Rb2D = GetComponent<Rigidbody2D>();
    }
   

    private void Start()
    {
        _controller = GetComponent<Controller2D>();

        //sprite gleda desno
        FaceDirection = true;

        //izracun gravitacije i brzine skoka
        _gravity = -(2 * JumpHeight) / Mathf.Pow(TimeToJumpApex, 2);
        _jumpVelocity = Mathf.Abs(_gravity) * TimeToJumpApex;
    }

    private void Update()
    {
        Controller();
    }

    private void FixedUpdate()
    {
    }

    private void Controller()
    {
        //s obzirom da se float događa on-hold buttona, neutralno mjesto za reset zastavice gdje se ne kolje s ostalim
        //potrebnim funkcionalnostima, ponajprije kontrolom brzine tijekom floata
        IsFloating = false;

        //dohvacanje inputa, okretanje lika
        Vector2 input = new Vector2(Input.GetAxisRaw("PlayerHorizontal"), Input.GetAxisRaw("PlayerVertical"));
        if (input.x > 0 && !FaceDirection)
            Flip();
        else if (input.x < 0 && FaceDirection)
            Flip();

        //zaustavljanje kod kolizije up-down
        if (_controller.Collisions.Above || _controller.Collisions.Below)
        {
            Velocity.y = 0;
        }

        //u principu IsGrounded()
        if (_controller.Collisions.Below)
        {
            CanJump = true;
            CanDoubleJump = false;
            CanTripleJump = false;
            CanDash = false;
            CanFloat = false;
        }

        //sve akcije. U ovom obliku, floatu je dan prioritet, i dok je on pritisnut, nema skoka ni dashanja
        if (Input.GetButton("PlayerFloat") && !_controller.Collisions.Below && CanFloat && Velocity.y < 0)
        {
            Float();
        }
        else if (Input.GetButtonDown("PlayerDash") && !_controller.Collisions.Below && CanDash)
        {
            Dash(ref input);
        }

        else if (Input.GetButtonDown("PlayerJump") && _controller.Collisions.Below && CanJump)
        {
            Jump();
        }
        else if (Input.GetButtonDown("PlayerJump") && !_controller.Collisions.Below && CanDoubleJump)
        {
            DoubleJump();
        }
        else if (Input.GetButtonDown("PlayerJump") && !_controller.Collisions.Below && CanTripleJump)
        {
            TripleJump();
        } 
        
        //standardni movement
        Movement(ref input);
    }

    private void Movement(ref Vector2 input)
    {
        //provjera stanja (pod, zrak ili padobran) i podesavanje parametara
        float targetVelocityX;
        float accelerationTime;
        if (_controller.Collisions.Below)
        {
            accelerationTime = AccelerationTimeGrounded;
            targetVelocityX = input.x * MoveSpeed;
        }
        else if (IsFloating)
        { 
            accelerationTime = AccelerationTimeFloating;
            targetVelocityX = input.x * FiniteFloatHorizontalSpeed;
        }
        else
        {
            accelerationTime = AccelerationTimeAirborne;
            targetVelocityX = input.x * MoveSpeed;
        }

        //kretanje lijevo-desno, ovako bi trebalo biti vise smooth (obviously)
        Velocity.x = Mathf.SmoothDamp(Velocity.x, targetVelocityX, ref _velocityXSmoothing, accelerationTime);
        Velocity.y += _gravity * Time.deltaTime;
        _controller.Move(Velocity * Time.deltaTime);
    }


    //funkcije za sve akcije

    protected void Jump()
    {
        Velocity.y = _jumpVelocity;
        CanDoubleJump = false;
        CanDoubleJump = true;
        CanDash = true;
        CanFloat = true;
    }

    protected void DoubleJump()
    {
        Velocity.y = _jumpVelocity * SecondaryJumpsFactor;
        CanDoubleJump = false;
        CanTripleJump = true;
    }

    protected void TripleJump()
    {
        Velocity.y = _jumpVelocity * SecondaryJumpsFactor;
        CanDoubleJump = false;
        CanTripleJump = false;
    }

    protected void Dash(ref Vector2 input)
    {
        Velocity.x = DashVelocity * input.x;
        Velocity.y = Velocity.y < 2 ? 2 : Velocity.y;
        CanDash = false;
    }

    protected void Float()
    {
        Velocity.y = (Velocity.y * 0.8f < -FiniteFloatVerticalSpeed) ? (Velocity.y * 0.8f) : -FiniteFloatVerticalSpeed;
        IsFloating = true;
    }

    protected void Flip()
    {
        FaceDirection = !FaceDirection;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
