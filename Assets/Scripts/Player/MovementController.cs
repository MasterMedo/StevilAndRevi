﻿using System.Collections;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    // kut nagiba koji se racuna kao "tlo" u svrhu resetanja mogucnosti skoka (grounded) 
    // Note to Self:
    // mislim da ce se dodavanjem slicnih parametara i igranjem s onCollision funkcijom moci
    // napraviti dosta zgodna i relativno jednostavna interakcija sa rotirajucim platformama i
    // vecinom drugih objekata u igri nakon sto se pravilno posloze tagovi
    private static readonly float GroundAngleTolerance = Mathf.Cos(30.0f * Mathf.Deg2Rad);

    // faktor za "steer" u zraku, i radna varijabla 
    public float airSteerFactor = 0.2f;
    [HideInInspector] public bool canDash;
    [HideInInspector] public bool canDoubleJump;
    [HideInInspector] public bool canFreeze;

    //spremljena brzina
    private Vector2 currentVellocity;


    [HideInInspector] public Vector2 currentVelocity;
    [HideInInspector] public bool dash;
    public float dashForce = 300f;
    [HideInInspector] public bool doubleJump;

    public float doubleJumpForce = 200f;

    //zastavice za detektiranje jump/doublejump/dash
    //i na koju je stranu lik okrenut (okretanje modela)
    [HideInInspector] public bool facingRight = true;

    private float factor = 1f;
    [HideInInspector] public bool freeze;
    [HideInInspector] public bool isGrounded;
    [HideInInspector] public bool jump;
    public float jumpForce = 300f;
    public float maxSpeed = 5f;

    //vrijednosti parametara (promjenjivo u inspectoru, ove vrijednosti u principu nebitne)
    public float moveForce = 10;

    private Vector2 preFreezeVellocity;
    public float projectileHitForce = 200f;

    //referenca na rigidBody za svu fiziku, dohvacamo u funkciji start
    private Rigidbody2D rb2d;

    private void Start()
    {
        //dohvati rigidBody i onemogući rotaciju lika (sumnjam da je želimo kao feature)
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.freezeRotation = true;
    }


    private void Update()
    {
        currentVelocity = rb2d.velocity;
        //ako pritisnemo jump button na tlu
        if (Input.GetButtonDown("PlayerJump") && isGrounded)
        {
            isGrounded = false;
            jump = true;
            canDoubleJump = true;
            canDash = true;
            canFreeze = true;
        }

        //ako pritisnemo jump button dok nismo na tlu, a jos nije iskoristen double jump
        else if (Input.GetButtonDown("PlayerJump") && canDoubleJump)
        {
            doubleJump = true;
        }

        //ako nismo na tlu i pritisnemo dash, a jos nismo dashali
        else if (Input.GetButtonDown("PlayerDash") && canDash)
        {
            dash = true;
        }

        //ako smo u zraku i jos nismo iskoristili Freeze
        else if (Input.GetButtonDown("PlayerFloat") && canFreeze)
        {
            freeze = true;
        }
    }


    private void FixedUpdate()
    {
        if (isGrounded)
            factor = 1f;
        else
            factor = airSteerFactor;

        // kontrola horizontalnog kretanja
        var h = Input.GetAxis("HorizontalPlayer");

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce * factor);

        //kontrola max brzine
        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);


        //okretanje lika s obzirom na smjer kretanja
        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        // logika skoka, duplog skoka i dasha, stvara impuls sile 
        // vrijednosti jumpForce/doubleJumpForce/dashForce prema gore/lijevo/desno
        if (jump)
        {
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }

        if (doubleJump)
        {
            rb2d.AddForce(new Vector2(0f, doubleJumpForce));
            doubleJump = false;
            canDoubleJump = false;
        }

        if (dash)
        {
            rb2d.AddForce(Vector2.right * h * dashForce);
            dash = false;
            canDash = false;
        }

        //logika Freeza, yield se koristi za kontrolu toka izvodenja
        if (freeze)
        {
            freeze = false;
            StartCoroutine(FreezeRoutine());
        }
    }

    //funkcija okretanja lika
    private void Flip()
    {
        facingRight = !facingRight;
        var theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


    //funkcija koja se poziva prilikom detekcije kolizije
    private void OnCollisionEnter2D(Collision2D coll)
    {
        foreach (var contact in coll.contacts)
            //provjerava tlo
            if (coll.gameObject.tag.Equals("Ground") && Vector2.Dot(contact.normal, Vector2.up) > GroundAngleTolerance)
                isGrounded = true;

            //kolizija s projektilom, pushback u smjeru normale, za sad jednak
            //za sve projektile, promjenjivo
            //else if (coll.gameObject.tag.Equals("Stalactite")
            //         || coll.gameObject.tag.Equals("Missile")
            //         || coll.gameObject.tag.Equals("Homing Missile"))
            //    rb2d.AddForce(contact.normal * projectileHitForce);
    }

    //routine za handleanje freeza
    private IEnumerator FreezeRoutine()
    {
        preFreezeVellocity = rb2d.velocity;
        rb2d.velocity = new Vector2(0, 0);
        rb2d.gravityScale = 0f;
        yield return new WaitForSecondsRealtime(0.5f);
        yield return StartCoroutine(AfterFreezeRoutine());
        rb2d.gravityScale = 1f;
    }

    //routine za "after-freeze" efekt smanjene gravitacije
    private IEnumerator AfterFreezeRoutine()
    {
        rb2d.velocity = new Vector2(preFreezeVellocity.x, 0);
        rb2d.gravityScale = 0.3f;
        yield return new WaitForSecondsRealtime(0.5f);
    }
}