﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStatic : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object
    public bool hasToMoveOnYAxis = false;
    private Camera cam;
    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();

        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
    }

    private void LateUpdate()
    {
        Vector3 viewPosition = cam.WorldToViewportPoint(player.transform.position);
        if(viewPosition.x > 0.99)
        {
            transform.position = new Vector3(player.transform.position.x + offset.x, hasToMoveOnYAxis == true ? player.transform.position.y : 0, transform.position.z);
        }
        else if(viewPosition.x < 0.01)
        {
            transform.position = new Vector3(player.transform.position.x + offset.x, hasToMoveOnYAxis == true ? player.transform.position.y : 0, transform.position.z);
        }
    }
}
