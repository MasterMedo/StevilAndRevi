﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenPlatformMovement : MonoBehaviour {

    private Rigidbody2D rb2d;
    public float speed;

    // Use this for initialization
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = 0;
        float moveVertical = Input.GetAxis("PlatformPositive");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        rb2d.AddForce(movement * speed);
    }
}
