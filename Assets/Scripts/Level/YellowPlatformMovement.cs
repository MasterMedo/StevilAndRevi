﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowPlatformMovement : MonoBehaviour {

    private Rigidbody2D rb2d;
    public float jumpPower = 1;

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetButtonDown("PlatformPositive") || Input.GetButtonDown("PlatformNegative"))
        {
            rb2d.AddForce(new Vector2(0, 10) * jumpPower, ForceMode2D.Impulse);
        }
    }
}
