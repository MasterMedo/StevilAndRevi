﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlatformMovement : MonoBehaviour {

    public HingeJoint2D hinge;
    JointMotor2D jointMotor;


    // Use this for initialization
    void Start () {
        hinge = GetComponent<HingeJoint2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("PlatformPositive") || Input.GetButtonDown("PlatformNegative"))
        {
            jointMotor.motorSpeed = -hinge.motor.motorSpeed;
            jointMotor.maxMotorTorque = 10000;
            hinge.motor = jointMotor;
            
        }
    }
}
