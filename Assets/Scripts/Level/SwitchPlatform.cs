﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPlatform : MonoBehaviour {

    public GreenPlatformMovement[] green;
    public YellowPlatformMovement[] yellow;
    public RedPlatformMovement[] red;
    public BluePlatformMovement[] blue;

    private int numberOfGreenPlatforms;
    private int numberOfYellowPlatforms;
    private int numberOfRedPlatforms;
    private int numberOfBluePlatforms;

    private int counter = 0;

    // Use this for initialization
    void Start () {
        green = GetComponentsInChildren<GreenPlatformMovement>();
        yellow = GetComponentsInChildren<YellowPlatformMovement>();
        red = GetComponentsInChildren<RedPlatformMovement>();
        blue = GetComponentsInChildren<BluePlatformMovement>();

        numberOfGreenPlatforms = green.Length;
        numberOfYellowPlatforms = yellow.Length;
        numberOfRedPlatforms = red.Length;
        numberOfBluePlatforms = blue.Length;

        //Pocetno postavljanje platformi koje se kontroliraju
        for (int i = 0; i < numberOfGreenPlatforms; i++)
        {
            green[i].enabled = false;
        }

        for (int i = 0; i < numberOfYellowPlatforms; i++)
        {
            yellow[i].enabled = false;
        }

        for(int i = 0; i < numberOfRedPlatforms; i++)
        {
            red[i].enabled = false;
        }

        for (int i = 0; i < numberOfBluePlatforms; i++)
        {
            blue[i].enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {

        //Pritiskom Tab-a mijenja se boja platformi koju kontroliramo, najbolje radi kad postoje sve 4 vrste
	    if (Input.GetButtonDown("Green Platform"))
	    {

	        for (int i = 0; i < numberOfGreenPlatforms; i++)
	        {
	            green[i].enabled = true;
	        }
	    }

	    if (Input.GetButtonDown("Yellow Platform"))
	    {

	        for (int i = 0; i < numberOfYellowPlatforms; i++)
	        {
	            yellow[i].enabled = true;
	        }
	    }

	    if (Input.GetButtonDown("Red Platform"))
	    {
	        for (int i = 0; i < numberOfRedPlatforms; i++)
	        {
	            red[i].enabled = true;
	        }
	    }
	    if (Input.GetButtonDown("Blue Platform"))
	    {
            for (int i = 0; i < numberOfBluePlatforms; i++)
                {
                    blue[i].enabled = true;
                }
        }
    }

    private void ResetAllPlatforms()
    {
        for (int i = 0; i < numberOfGreenPlatforms; i++)
        {
            green[i].enabled = false;
        }
        for (int i = 0; i < numberOfYellowPlatforms; i++)
        {
            yellow[i].enabled = false;
        }
        for (int i = 0; i < numberOfRedPlatforms; i++)
        {
            red[i].enabled = false;
        }
        for (int i = 0; i < numberOfBluePlatforms; i++)
        {
            blue[i].enabled = false;
        }
    }
}
