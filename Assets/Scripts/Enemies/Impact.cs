﻿using System;
using UnityEngine;

public class Impact : MonoBehaviour
{

    public float DestroyDelay = 0.0f;
    public float DamagePercent = 0.0f;

    public GameObject ExplosionPrefab;

    void OnCollisionEnter2D(Collision2D collision)
    {
        Transform curTransform = transform;

        Destroy(gameObject, DestroyDelay);

        if (collision.gameObject.CompareTag("Player"))
        {
            Health playerHealth = collision.gameObject.GetComponent<Health>();
            playerHealth.ApplyDamage(DamagePercent / 100.0f * playerHealth.RespawnHealthPoints);
        }

        if (ExplosionPrefab != null)
        {
            Instantiate(ExplosionPrefab, curTransform.localPosition, curTransform.localRotation);
        }
    }
}
