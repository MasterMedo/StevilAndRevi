﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterBehaviourOld : MonoBehaviour
{

    public float DelayTime = 2.0f;
    public float SpawnTime = 0.1f;

    public float SpawnPercent = 30.0f;

    public GameObject MissilePrefab;
    public GameObject HomingMissilePrefab;

    public float HomingPercent = 0.0f;

    private float _lastSpawnTime;
    private float _lastTryTime;

    private GameObject player;

    // Use this for initialization
    void Start ()
	{
	    _lastSpawnTime = Time.time;
	    
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
	    if (Time.time - _lastSpawnTime < DelayTime)
	    {
	        return;
	    }

	    if (Time.time - _lastTryTime < SpawnTime)
	    {
	        return;
	    }

	    if (Random.value > SpawnPercent / 100.0f)
	    {
	        _lastTryTime = Time.time;
	        return;
	    }

	    _lastSpawnTime = Time.time;

        Vector3 position = transform.position + 
            (player.GetComponent<Transform>().position.x < transform.position.x ? 
                Vector3.left : Vector3.right) * 2f;

        Quaternion rotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

        GameObject newInstance = (Random.value < HomingPercent / 100.0f ? HomingMissilePrefab: MissilePrefab);

	    Instantiate(newInstance, position, rotation);
	}
}
