﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MissileBehaviourOld : MonoBehaviour
{

    public float Angle = 30.0f;

    public float Speed = 10.0f;
    public float Precision = 3.0f;

    public bool IsHoming = false;

    private Rigidbody2D _playerRigidbody;

    private Transform _myTransform;
    private Rigidbody2D _myRigidbody;

    private Vector2 _target;

	// Use this for initialization
	void Start ()
	{
	    _myTransform = gameObject.GetComponent<Transform>();
	    _myRigidbody = gameObject.GetComponent<Rigidbody2D>();

        _myTransform.Rotate(Vector3.forward * Angle);

        _playerRigidbody = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();

	    _target = _playerRigidbody.position + _playerRigidbody.velocity / Precision;

	    Vector2 currentDir = (_target - _myRigidbody.position).normalized;
	    _target += currentDir * 100.0f;
	}
	
	// Update is called once per frame
	void Update () {
	    if (IsHoming)
	    {
	        _target = _playerRigidbody.position + _playerRigidbody.velocity / Precision;
        }

	    Vector2 start = _myRigidbody.position;

	    _myRigidbody.velocity = (_target - start).normalized * Speed;
        _myTransform.LookAt(_target);
	}
}
