﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class StalactiteBehaviour : MonoBehaviour
{

    public float FallingDelay = 0.1f;

    private Transform _myTransform;
    private Rigidbody2D _myRigidbody;
    private PolygonCollider2D _myCollider;

    private Transform _playerTransform;
    private bool _playerDetected;

    private float _detectedTime;

    private float _lowerBound;
    private float _upperBound;

	// Use this for initialization
	void Start ()
	{
	    _myRigidbody = gameObject.GetComponent<Rigidbody2D>();
	    _myTransform = gameObject.GetComponent<Transform>();
	    _myCollider = gameObject.GetComponent<PolygonCollider2D>();

        _myRigidbody.gravityScale = 0.0f;
	    _playerDetected = false;

	    _playerTransform = GameObject.FindWithTag("Player").GetComponent<Transform>();

	    Vector3 myPosition = _myTransform.localPosition;
	    Vector3 myScale = _myTransform.localScale;

	    Vector2[] myHitbox = _myCollider.points;

	    float mini = 100.0f;
	    float maxi = -100.0f;
	    for (int i = 0; i < myHitbox.Length; i++)
	    {
	        mini = Mathf.Min(mini, myHitbox[i].x);
	        maxi = Mathf.Max(maxi, myHitbox[i].x);
        }

	    _lowerBound = myPosition.x + mini * myScale.x;
	    _upperBound = myPosition.x + maxi * myScale.x;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    Vector3 position = _playerTransform.localPosition;

	    if (!_playerDetected && 
            _lowerBound <= position.x && position.x <= _upperBound)
	    {
	        _playerDetected = true;
	        _detectedTime = Time.time;
	    }

	    if (_playerDetected &&
	        Time.time - _detectedTime >= FallingDelay)
	    {
	        _myRigidbody.gravityScale = 1.0f;
	    }
	}
}
