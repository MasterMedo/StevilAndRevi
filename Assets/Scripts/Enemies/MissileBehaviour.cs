﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MissileBehaviour : MonoBehaviour
{
    public float Angle = 30.0f;
    public float Precision = 1.0f;
    public float Speed = 1.0f;

    public bool IsHoming = false;

    private Rigidbody2D _rb2D;
    private Transform _transform;

    private GameObject _player;
    private Rigidbody2D _playerRb2D;
    private Transform _playerTransform;
    private Vector2 _target;

	void Start ()
	{
	    _rb2D = gameObject.GetComponent<Rigidbody2D>();
	    _transform = gameObject.GetComponent<Transform>();

        _player = GameObject.FindGameObjectWithTag("Player");
	    _playerTransform = _player.GetComponent<Transform>();
	    _playerRb2D = _player.GetComponent<Rigidbody2D>();

	    _target = _playerRb2D.position + Precision * _playerRb2D.velocity;
        _rb2D.AddForce(Speed * (_target - _rb2D.position).normalized);
	}
	
	void FixedUpdate () {
	    if (IsHoming)
	    {
	        _target = _playerRb2D.position + Precision * _playerRb2D.velocity;
        }
        _transform.LookAt(_playerTransform);
	    _rb2D.AddForce(Speed * (_target - _rb2D.position).normalized);
    }
}
