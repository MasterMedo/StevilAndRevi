﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutoDestroy : MonoBehaviour
{

    private ParticleSystem _ps;

	// Use this for initialization
	void Start ()
	{
	    _ps = gameObject.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (_ps)
        {
            if (!_ps.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
